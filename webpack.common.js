const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        app: './src/js/app.js'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/index.html',
            favicon: './src/favicon.png',
            minify: {
                collapseWhitespace: true,
                removeComments: true
            },
            chunks: ['app']
        }),
        new ExtractTextPlugin({
            filename: 'main.bundle.css',
            disable: false,
            allChunks: true,
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            Popper: ['popper.js', 'default']
        })
    ],
    module: {
        rules: [
            {
                test: /\.html$/,
                include: path.resolve(__dirname, "src"),
                exclude: /(node_modules)/,
                use: ['html-loader']
            },
            {
                test: /\.js$/,
                include: path.resolve(__dirname, "src"),
                exclude: /(node_modules)/,
                use:[{
                    loader: "babel-loader",
                    options: {
                        presets: ['babel-preset-env']
                    }
                }]                
            },
            {
                test: /\.(css|scss)$/,
                include: path.resolve(__dirname, "src"),
                exclude: /(node_modules)/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader/url!file-loader',
                    use: [{
                        loader: 'css-loader',
                    }, {
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                ];
                            }
                        }
                    }, {
                        loader: 'sass-loader'
                    }]
                })
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                include: path.resolve(__dirname, "src"),
                exclude: /(node_modules)/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'assets/',
                    }
                }]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                include: path.resolve(__dirname, "src"),
                exclude: /(node_modules)/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'assets/',
                    }
                }]
            }
        ]
    }
};